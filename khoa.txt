Installation and Configuration
These configuration settings are commonly used when configuring a Linux package installation. For a complete list of settings, see the README file.

Installing GitLab.
Manually downloading and installing a GitLab package.
Setting up a domain name/URL for the GitLab Instance so that it can be accessed easily.
Enabling HTTPS.
Enabling notification emails.
Enabling replying via email.
Installing and configuring Postfix.
Enabling container registry on GitLab.
You require SSL certificates for the domain used for container registry.
Enabling GitLab Pages.
If you want HTTPS enabled, you must get wildcard certificates.
Enabling Elasticsearch.
GitLab Mattermost. Set up the Mattermost messaging app that ships with the Linux package.
GitLab Prometheus Set up the Prometheus monitoring included in the Linux package.
GitLab High Availability Roles.